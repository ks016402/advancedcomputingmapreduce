package Objective4;

import lib.ErrorCheck;
import lib.KeyValPair;
import lib.Mapper;

import java.util.ArrayList;

public class Distance_Mapper extends Mapper {
	@Override
	protected ArrayList<KeyValPair> map(String line) {


		String[] values = line.split(",");

		ArrayList<KeyValPair> results = new ArrayList<>();

		if (values[0] != line) {
			String passengerID = values[0];
			String flightID = values[1];
			String fAirportCode = values[2];
			String dAirportCode = values[3];

			if (ErrorCheck.airport(dAirportCode) && ErrorCheck.airport(fAirportCode)) {
				if (ErrorCheck.flight(flightID)) {
					results.add(new KeyValPair(flightID, fAirportCode + "|" + dAirportCode));
				}
				if (ErrorCheck.passenger(passengerID)) {
					results.add(new KeyValPair(passengerID, fAirportCode + "|" + dAirportCode));
				}
			}
		}

		return results;
	}
}
