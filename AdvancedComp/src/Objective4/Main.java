package Objective4;

import lib.Job;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class Main {
	public static void main(String[] args) throws Exception {
		// Calculate the lmiles for each flight and the total travelled by each passenger

		Job job = new Job();

		job.setMapper(Distance_Mapper.class);
		job.setReducer(Distance_Reducer.class);

		BufferedReader input = new BufferedReader(new FileReader("A:\\AdvancedComp\\AComp_Passenger_data.csv"));
		BufferedWriter output = new BufferedWriter(new FileWriter("A:\\AdvancedComp\\distances.txt"));

		job.setInput(input);
		job.setOutput(output);

		job.run();
	}
}
