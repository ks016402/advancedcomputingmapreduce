package Objective3;

import lib.Job;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

class Main {

	public static void main(String[] args) throws Exception {
		Job job = new Job();

		job.setMapper(PassengerCountMapper.class);
		job.setReducer(PassengerCountReducer.class);

		BufferedReader input = new BufferedReader(new FileReader("A:\\AdvancedComp\\AComp_Passenger_data.csv"));
		BufferedWriter output = new BufferedWriter(new FileWriter("A:\\AdvancedComp\\passenger-numbers.txt"));
		job.setInput(input);
		job.setOutput(output);

		job.run();
	}
}
