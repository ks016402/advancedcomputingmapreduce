package Objective3;

import lib.ErrorCheck;
import lib.KeyValPair;
import lib.Mapper;

import java.util.ArrayList;

public class PassengerCountMapper extends Mapper {

	protected ArrayList<KeyValPair> map(String line) {


		String[] values = line.split(",");

		ArrayList<KeyValPair> finalResults = new ArrayList<>();

		if (values[0] != line) {
			String flightID = values[1];

			if (ErrorCheck.flight(flightID)) {
				finalResults.add(new KeyValPair(flightID, String.valueOf(1)));
			}
		}

		return finalResults;
	}
}
