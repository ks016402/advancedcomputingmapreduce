package Objective2;

import lib.KeyValPair;
import lib.Mapper;

import java.util.ArrayList;

public class FlightInfo_Mapper extends Mapper {
	@Override
	protected ArrayList<KeyValPair> map(String line) {


		String[] values = line.split(",");

		ArrayList<KeyValPair> results = new ArrayList<>();

		if (!values[0].equals(line)) {
			boolean isvalid = true;

			String passengerID = values[0];
			isvalid &= lib.ErrorCheck.passenger(passengerID);

			String flightID = values[1];
			isvalid &= lib.ErrorCheck.flight(flightID);

			String fAirportCode = values[2];
			isvalid &= lib.ErrorCheck.airport(fAirportCode);

			String dAirportCode = values[3];
			isvalid &= lib.ErrorCheck.airport(dAirportCode);

			String departureTime = values[4];
			isvalid &= lib.ErrorCheck.time(departureTime);

			String flightDuration = values[5];
			isvalid &= lib.ErrorCheck.duration(flightDuration);

			if (isvalid) {
				results.add(new KeyValPair(flightID, String.format("%s,%s,%s,%s,%s", passengerID, fAirportCode, dAirportCode, departureTime, flightDuration)));
			}
		}

		return results;
	}
}
