package Objective1;

import lib.KeyValPair;
import lib.Mapper;
import lib.ErrorCheck;

import java.util.ArrayList;

public class AirportCount_Mapper extends Mapper {
	protected ArrayList<KeyValPair> map(String line) {


		String[] values = line.split(",");

		ArrayList<KeyValPair> finalResults = new ArrayList<>();

		if (values[0] != line) {
			String flight = values[1];
			String fcode = values[2];

			if (ErrorCheck.airport(fcode) && ErrorCheck.flight(flight)) {
				finalResults.add(new KeyValPair(fcode, flight));
			}

		}

		return finalResults;

	}
}
