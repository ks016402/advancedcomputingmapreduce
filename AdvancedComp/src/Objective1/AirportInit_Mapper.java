package Objective1;

import lib.KeyValPair;
import lib.Mapper;
import lib.ErrorCheck;

import java.util.ArrayList;

public class AirportInit_Mapper extends Mapper {
	@Override
	protected ArrayList<KeyValPair> map(String line) {


		String[] values = line.split(",");

		ArrayList<KeyValPair> results = new ArrayList<>();

		if (values[0] != line) {
			String fromAirportCode = values[1];

			if (ErrorCheck.airport(fromAirportCode)) {
				results.add(new KeyValPair(fromAirportCode, String.valueOf(0)));
			}
		}

		return results;
	}
}
