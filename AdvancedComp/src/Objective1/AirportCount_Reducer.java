package Objective1;

import lib.KeyValPair;
import lib.Reducer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class AirportCount_Reducer extends Reducer {
	@Override
	protected KeyValPair reduce(String key, ArrayList<String> values) {


		Set<String> uniques = new HashSet<>();
		uniques.addAll(values);
		return new KeyValPair(key, String.valueOf(uniques.size()));
	}
}
