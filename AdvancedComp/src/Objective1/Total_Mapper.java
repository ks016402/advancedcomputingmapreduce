package Objective1;

import lib.KeyValPair;
import lib.Mapper;
import lib.ErrorCheck;

import java.util.ArrayList;

public class Total_Mapper extends Mapper {

	@Override
	protected ArrayList<KeyValPair> map(String line) {
		// Headers	: Airport, Flights
		// Line		: DEN,1
		// Output	: <"DEN", "1">

		String[] values = line.split(",");

		ArrayList<KeyValPair> FinalResults = new ArrayList<>();

		if (values[0] != line) {
			String code = values[0];
			String subtotal = values[1];

			if (ErrorCheck.airport(code)) {
				FinalResults.add(new KeyValPair(code, subtotal));
			}
		}

		return FinalResults;
	}
}
