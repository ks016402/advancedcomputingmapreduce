package Objective1;

import lib.Job;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

class Main {

	public static void main(String[] args) throws Exception {
		Main.job1();
		Main.job2();
		Main.job3();
	}

	// Lists all existing airports as having a total flight count of 0.
	public static void job1() throws Exception {
		Job job = new Job();

		job.setMapper(AirportInit_Mapper.class);
		job.setReducer(AirportInit_Reducer.class);

		BufferedReader input = new BufferedReader(new FileReader("A:\\AdvancedComp\\Top30airportlatlong.csv"));
		BufferedWriter output = new BufferedWriter(new FileWriter("A:\\AdvancedComp\\flightcount.txt"));

		job.setInput(input);
		job.setOutput(output);

		job.run();
	}

	// Appends the zero-totals with the real values from the data.
	public static void job2() throws Exception {
		Job job = new Job();

		job.setMapper(AirportCount_Mapper.class);
		job.setReducer(AirportCount_Reducer.class);

		BufferedReader input = new BufferedReader(new FileReader("A:\\AdvancedComp\\AComp_Passenger_data.csv"));
		BufferedWriter output = new BufferedWriter(new FileWriter("A:\\AdvancedComp\\flightcount2.txt", true));
		job.setInput(input);
		job.setOutput(output);

		job.run();
	}

	// Combines the totals.
	public static void job3() throws Exception {
		Job job = new Job();

		job.setMapper(Total_Mapper.class);
		job.setReducer(Total_Reducer.class);

		BufferedReader input = new BufferedReader(new FileReader("A:\\AdvancedComp\\flightcount2.txt"));
		BufferedWriter output = new BufferedWriter(new FileWriter("A:\\AdvancedComp\\flightcount3.txt"));
		job.setInput(input);
		job.setOutput(output);

		job.run();
	}

}
