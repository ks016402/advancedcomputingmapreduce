package Objective1;

import lib.KeyValPair;
import lib.Reducer;

import java.util.ArrayList;

public class AirportInit_Reducer extends Reducer {
	@Override
	protected KeyValPair reduce(String key, ArrayList<String> values) {
		int counter = 0;

		for (String val : values) {
			counter += Integer.parseInt(val, 10);
		}

		return new KeyValPair(key, String.valueOf(counter));
	}
}
